var User = require('../app/models/user');
var Neraca = require('../app/models/neraca');
var Logs = require('../app/models/logs');
var helper = require('../helper');
var config = helper.genConfig('app');
var accounting = require('accounting');
var requestIp = require('request-ip');
var fs = require('fs');
var multer  = require('multer');
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './public/files/');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + getExt(file.originalname));
    }
});
var upload = multer({ storage: storage  }).single('upload');

module.exports = function(app, passport) {

    // user auth request

    app.get('/logout', function(req, res) {
        req.logout();
        res.redirect('/');
    });

    app.get('/forgot', function(req, res) {
        res.redirect('/');
    });

    app.get('/login', function(req, res) {
        res.render('login.ejs', { appname: config.appname, message: req.flash('loginMessage') });
    });


    app.get('/signup', function(req, res) {
        res.render('signup.ejs', { appname: config.appname, message: req.flash('signupMessage') });
    });


    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/',
        failureRedirect : '/login',
        failureFlash : true
    }));


    app.post('/signup', passport.authenticate('local-signup', {
        successRedirect : '/',
        failureRedirect : '/signup',
        failureFlash : true
    }));


    // get request render view

    app.get('/', isLoggedIn, lastLogin, function(req, res) {
        res.render('index.ejs', {appname: config.appname, user : req.user, content: 'home'});
    });

    app.get('/pendapatan', isLoggedIn, lastLogin,function(req, res) {
        var id = req.user._id;

            Neraca
                .find({owner: id})
                //.sort('-createdAt')
                .sort('date')
                .exec(function(err, neracas){
                    if(err){
                        console.log(err);
                    }
                    var chart = [];
                    var total1 = 0;
                    var total2 = 0;
                    var total3 = 0;
                    var total = 0;
                    neracas.forEach(function(neraca){
                        if(neraca.category.charAt(0) == '1'){
                            if(neraca.category.charAt(1) == '1'){
                                 total1+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '2'){
                                total2+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '3'){
                                total3+= parseInt(neraca.amount);
                            }
                            total += parseInt(neraca.amount);
                            var data = {
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(total),
                                Keterangan: neraca.description,
                                Dana: total
                            };
                            chart.push(data);
                        }
                    });
                    res.render('pendapatan.ejs', {
                        appname: config.appname,
                        user : req.user,
                        pendapatan1: formatRupiah(total1),
                        persenPendapatan1: (total1 / total) * 100,
                        pendapatan2: formatRupiah(total2),
                        persenPendapatan2: (total2 / total) * 100,
                        pendapatan3: formatRupiah(total3),
                        persenPendapatan3: (total3 / total) * 100,
                        pendapatan4: formatRupiah(total),
                        chart: chart,
                        content: 'pendapatan'
                    });
                });
    });

    app.get('/pembelanjaan', isLoggedIn, lastLogin,function(req, res) {
        var id = req.user._id;
            Neraca
                .find({owner: id})
                .sort('date')
                .exec(function(err, neracas){
                    if(err){
                        console.log(err);
                    }
                    var chart = [];
                    var total1 = 0;
                    var total2 = 0;
                    var total3 = 0;
                    var total4 = 0;
                    var total5 = 0;
                    var total = 0;
                    neracas.forEach(function(neraca){
                        if(neraca.category.charAt(0) == '2'){
                            if(neraca.category.charAt(1) == '1'){
                                total1+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '2'){
                                total2+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '3'){
                                total3+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '4'){
                                total4+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '5'){
                                total5+= parseInt(neraca.amount);
                            }
                            total += parseInt(neraca.amount);
                            var data = {
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(total),
                                Keterangan: neraca.description,
                                Dana: total
                            };

                            chart.push(data);
                        }
                    });
                    res.render('pembelanjaan.ejs', {
                        appname: config.appname,
                        user : req.user,
                        pembelanjaan1: formatRupiah(total1),
                        persenPembelanjaan1: (total1 / total) * 100,
                        pembelanjaan2: formatRupiah(total2),
                        persenPembelanjaan2: (total2 / total) * 100,
                        pembelanjaan3: formatRupiah(total3),
                        persenPembelanjaan3: (total3 / total) * 100,
                        pembelanjaan4: formatRupiah(total4),
                        persenPembelanjaan4: (total4 / total) * 100,
                        pembelanjaan5: formatRupiah(total5),
                        persenPembelanjaan5: (total5 / total) * 100,
                        pembelanjaan6: formatRupiah(total),
                        chart: chart,
                        content: 'pembelanjaan'
                    });
                });
    });

    app.get('/pembiayaan', isLoggedIn, lastLogin,function(req, res) {
        var id = req.user._id;

            Neraca
                .find({owner: id})
                .sort('date')
                .exec(function(err, neracas){
                    if(err){
                        console.log(err);
                    }
                    var chart = [];
                    var total1 = 0;
                    var total2 = 0;
                    var total3 = 0;
                    var total4 = 0;
                    var total5 = 0;
                    var total = 0;
                    neracas.forEach(function(neraca){
                        if(neraca.category.charAt(0) == '2'){
                            if(neraca.category.charAt(1) == '1'){
                                total1+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '2'){
                                total2+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '3'){
                                total3+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '4'){
                                total4+= parseInt(neraca.amount);
                            }
                            if(neraca.category.charAt(1) == '5'){
                                total5+= parseInt(neraca.amount);
                            }
                            total += parseInt(neraca.amount);
                            var data = {
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(total),
                                Keterangan: neraca.description,
                                Dana: total
                            };
                            console.log(neraca,total);
                            chart.push(data);
                        }
                    });
                    res.render('pembiayaan.ejs', {
                        appname: config.appname,
                        user : req.user,
                        pembiayaan1: formatRupiah(total1),
                        persenPembiayaan1: (total1 / total) * 100,
                        pembiayaan2: formatRupiah(total2),
                        persenPembiayaan2: (total2 / total) * 100,
                        pembiayaan3: formatRupiah(total),
                        chart: chart,
                        content: 'pembiayaan'
                    });
                });
    });

    app.get('/profile', isLoggedIn, lastLogin,function(req, res) {

        var filterData;
        if(req.user.local.level == 'Admin' || req.user.local.level == 'SuperAdmin' ){
            filterData = {$match: {}};
        }else{
            filterData = {$match: {user : req.user._id}};
        }

        var groupData = {$group: {
            _id : {
                year: { $year : "$createdAt" },
                month: { $month : "$createdAt" },
                day: { $dayOfMonth : "$createdAt" }
            },
            logs: {
                $push: {
                    owner: '$user',
                    action: '$action',
                    victimUser: '$victimUser',
                    victimNeraca: '$victimNeraca',
                    createdAt: '$createdAt'
                }
            }}};
        var projectData = {$project: {logs: 1}};
        var sortData = {$sort: { createdAt: 1 }};

        var agregatePipe = [
            filterData,
            sortData,
            groupData,
            projectData
        ];

        Logs.aggregate(agregatePipe,function(err, logs){
            if(err){
                console.log(err)
            }
            console.log(logs);
            res.render('profile.ejs', {
                appname: config.appname,
                user: req.user,
                logs: logs,
                config: config,
                content: 'profile',
                err: req.flash('err'),
                success: req.flash('success'),
                tips: req.flash('tips'),
                warning: req.flash('warning')
            });
        });
    });

    app.get('/perencanaan', isLoggedIn, lastLogin, function(req, res) {
        var id = req.user._id;

            Neraca
                .find({owner: id})
                .sort('createdAt')
                .exec(function(err, neracas){
                    if(err){
                        console.log(err);
                    }
                    var pendapatan = [];
                    var totalPenerimaanPembiayaan = 0;
                    var totalPengeluaranPembiayaan = 0;
                    var totalPembelanjaan = 0;
                    var totalPendapatan = 0;
                    neracas.forEach(function(neraca){
                        if(neraca.category.charAt(0) == '1'){
                            totalPendapatan += parseInt(neraca.amount);
                        }
                        if(neraca.category.charAt(0) == '2'){
                            totalPembelanjaan += parseInt(neraca.amount);
                        }
                        if(neraca.category.charAt(0) == '3'){
                            if(neraca.category.charAt(1) == '1'){
                                totalPenerimaanPembiayaan += parseInt(neraca.amount);
                            }else if(neraca.category.charAt(1) == '2'){
                                totalPengeluaranPembiayaan += parseInt(neraca.amount);
                            }

                        }
                        pendapatan.push(neraca);
                    });
                    res.render('perencanaan.ejs', {
                        err: req.flash('err'),
                        success: req.flash('success'),
                        tips: req.flash('tips'),
                        warning: req.flash('warning'),
                        appname: config.appname,
                        user : req.user,
                        pendapatan: pendapatan,
                        totalPendapatan: formatRupiah(totalPendapatan),
                        totalPembelanjaan: formatRupiah(totalPembelanjaan),
                        totalPenerimaanPembiayaan: formatRupiah(totalPenerimaanPembiayaan),
                        totalPengeluaranPembiayaan: formatRupiah(totalPengeluaranPembiayaan),
                        content: 'perencanaan',
                        category : [
                            {text: '1.1 Pendapatan Hasil Desa', children: [
                                {text: 'Hasil Usaha', id: '1110'},
                                {text: 'Swadaya, Partisipasi dan Gotong Royong', id: '1120'},
                                {text: 'Lain-Lain Pendapatan Asli Desa yang Sah', id: '1130'}
                            ]},
                            {text: '1.2 Pendapatan Transfer', children: [
                                {text: 'Dana Desa', id: '1210'},
                                {text: 'Bagian dari Hasil Pajak dan Retribusi Daerah Kabupaten / Kota', id: '1220'},
                                {text: 'Alokasi Dana Desa', id: '1230'},
                                {text: 'Bantuan Keuangan', id: '1240'},
                                {text: 'Bantuan Provinsi', id: '1241'},
                                {text: 'Bantuan Kabupaten / Kota', id: '1242'}
                            ]},
                            {text: '1.3 Pendapatan Lain-Lain', children: [
                                {text: 'Hibah dan Sumbangan dari Pihak Ke-3 yang Tidak Mengikat', id: '1310'},
                                {text: 'Lain-Lain Pendapatan Desa yang Sah', id: '1320'}
                            ]},
                            {text: '2.1.1 Penghasilan Tetap dan Tunjangan', children: [
                                {text: 'Belanja Pegawai (2111)', id: '2111'}
                            ]},
                            {text: '2.1.2 Operasional Perkantoran', children: [
                                {text: 'Belanja Barang dan Jasa (2122)', id: '2122'},
                                {text: 'Belanja Modal (2123)', id: '2123'}
                            ]},
                            {text: '2.1.3 Operasional BPD', children: [
                                {text: 'Belanja Barang dan Jasa (2132)', id: '2132'}
                            ]},
                            {text: '2.1.4 Operasional RT/RW', children: [
                                {text: 'Belanja Barang dan Jasa (2142)', id: '2142'}
                            ]},
                            {text: '2.2.1 Perbaikan Saluran Irigasi', children: [
                                {text: 'Belanja Barang dan Jasa (2212)', id: '2212'},
                                {text: 'Belanja Modal (2213)', id: '2213'}
                            ]},
                            {text: '2.2.2 Pengaspalan Jalan Desa', children: [
                                {text: 'Belanja Barang dan Jasa (2222)', id: '2222'},
                                {text: 'Belanja Modal (2223)', id: '2223'}
                            ]},
                            {text: 'Kegiatan (2230)', id: '2230'},
                            {text: '2.3.1 Kegiatan Pembinaan Ketentraman dan Ketertiban', children: [
                                {text: 'Belanja Barang dan Jasa (2312)', id: '2312'}
                            ]},
                            {text: 'Kegiatan (2320)', id: '2320'},
                            {text: '2.4.1 Kegiatan Pemberdayaan Masyarakat', children: [
                                {text: 'Belanja Barang dan Jasa (2412)', id: '2412'}
                            ]},
                            {text: 'Kegiatan (2420)', id: '2420'},
                            {text: '2.5.1 Kejadian Luar Biasa', children: [
                                {text: 'Belanja Barang dan Jasa (2512)', id: '2512'}
                            ]},
                            {text: 'Kegiatan (2520)', id: '2520'},
                            {text: '3.1 Penerimaan Pembiayaan', children: [
                                {text: 'SILPA (3110)', id: '3110'},
                                {text: 'Pencairan Dana Cadangan (3120)', id: '3120'},
                                {text: 'Hasil Kekayaan Desa yang Dipisahkan (3130)', id: '3130'}
                            ]},
                            {text: '3.1 Pengeluaran Pembiayaan', children: [
                                {text: 'Pembentukan Dana Cadangan (3210)', id: '3210'},
                                {text: 'Penyertaan Modal Desa (3220)', id: '3220'}
                            ]}
                        ]
                    });
                });
    });

    app.get('/buku', isLoggedIn, lastLogin,function(req, res) {
        var id = req.user._id;
        var filterData = {$match: {owner: id}};
        var sortData = {$sort: {date: 1}};
        var grupData = {$group: {_id : {$year : '$date'}}};
        var aggregatePipe = [filterData,grupData,sortData];
        Neraca.aggregate(aggregatePipe,function(err, datas){
            if (err) {
                console.log(err)
            }
            var tahun = [];
            console.log(datas);
            datas.forEach(function(data){
                tahun.push({text: data._id.toString(), id: data._id.toString()});
            });
            res.render('buku.ejs', {
                appname: config.appname,
                user: req.user,
                content: 'buku',
                year: tahun
            });
        });
    });

    app.get('/print/:tahun',isLoggedIn, lastLogin,function(req, res){
        var id = req.user._id;
        var tahunSekarang = new Date(req.params.tahun,0,0);
        var tahunDepan = new Date(req.params.tahun+1,0,0);
        var filterData = {$match: {
            $and: [
                {owner : id},
                {date: { $gte: tahunSekarang,$lt: tahunDepan}}
            ]}};
        var grupData = {$group: {
            _id: '$category',
            total: {$sum: '$amount'},
            description: {$push: '$description'},
            countRecord: {$sum: 1}
        }};
        var sortData = {$sort: {_id: 1}};
        var aggregatePipe = [filterData,grupData,sortData];
        var category = [
            {text: 'Hasil Usaha', id: '1110'},
            {text: 'Swadaya, Partisipasi dan Gotong Royong', id: '1120'},
            {text: 'Lain-Lain Pendapatan Asli Desa yang Sah', id: '1130'},
            {text: 'Dana Desa', id: '1210'},
            {text: 'Bagian dari Hasil Pajak dan Retribusi Daerah Kabupaten / Kota', id: '1220'},
            {text: 'Alokasi Dana Desa', id: '1230'},
            {text: 'Bantuan Keuangan', id: '1240'},
            {text: 'Bantuan Provinsi', id: '1241'},
            {text: 'Bantuan Kabupaten / Kota', id: '1242'},
            {text: 'Hibah dan Sumbangan dari Pihak Ke-3 yang Tidak Mengikat', id: '1310'},
            {text: 'Lain-Lain Pendapatan Desa yang Sah', id: '1320'},
            {text: 'Belanja Pegawai (2111)', id: '2111'},
            {text: 'Belanja Barang dan Jasa (2122)', id: '2122'},
            {text: 'Belanja Modal (2123)', id: '2123'},
            {text: 'Belanja Barang dan Jasa (2132)', id: '2132'},
            {text: 'Belanja Barang dan Jasa (2142)', id: '2142'},
            {text: 'Belanja Barang dan Jasa (2212)', id: '2212'},
            {text: 'Belanja Modal (2213)', id: '2213'},
            {text: 'Belanja Barang dan Jasa (2222)', id: '2222'},
            {text: 'Belanja Modal (2223)', id: '2223'},
            {text: 'Kegiatan (2230)', id: '2230'},
            {text: 'Belanja Barang dan Jasa (2312)', id: '2312'},
            {text: 'Kegiatan (2320)', id: '2320'},
            {text: 'Belanja Barang dan Jasa (2412)', id: '2412'},
            {text: 'Kegiatan (2420)', id: '2420'},
            {text: 'Belanja Barang dan Jasa (2512)', id: '2512'},
            {text: 'Kegiatan (2520)', id: '2520'},
            {text: 'SILPA (3110)', id: '3110'},
            {text: 'Pencairan Dana Cadangan (3120)', id: '3120'},
            {text: 'Hasil Kekayaan Desa yang Dipisahkan (3130)', id: '3130'},
            {text: 'Pembentukan Dana Cadangan (3210)', id: '3210'},
            {text: 'Penyertaan Modal Desa (3220)', id: '3220'}
        ]
        Neraca.aggregate(aggregatePipe,function(err, neracas) {
            if (err) {
                console.log(err)
            }
            var totalPendapatan = 0;
            var totalPembelanjaan = 0;
            var totalSurplus = 0;
            var totalDefisit = 0;
            neracas.forEach(function(neraca){
                if(neraca._id.toString().charAt(0) == '1'){
                    totalPendapatan += neraca.total;
                }
                if(neraca._id.toString().charAt(0) == '2'){
                    totalPembelanjaan += neraca.total;
                }
                if(neraca._id.toString().charAt(0) == '3' && neraca._id.toString().charAt(1) == '1'){
                    totalSurplus += neraca.total;
                }
                if(neraca._id.toString().charAt(0) == '3' && neraca._id.toString().charAt(1) == '2'){
                    totalDefisit += neraca.total;
                }
                neraca.total = formatRupiah(neraca.total);
                category.forEach(function(kategori){
                    if(neraca._id.toString() == kategori.id){
                        neraca.text = kategori.text;
                    }
                });
            });
            console.log(neracas);
            res.render('print.ejs', {
                appname: config.appname,
                user: req.user,
                content: 'print preview',
                records: neracas,
                pendapatan: formatRupiah(totalPendapatan),
                pembelanjaan: formatRupiah(totalPembelanjaan),
                surplus: formatRupiah(totalSurplus),
                defisit: formatRupiah(totalDefisit)
            });
        });
    });

    app.get('/bantuan', isLoggedIn, lastLogin,function(req, res) {
        res.render('content.ejs', {appname: config.appname, user : req.user, content: 'bantuan'});
    });

    app.get('/user', isLoggedIn, isAdmin, function(req, res){
        User
            .find({'local.level' : {$nin: ['SuperAdmin','Admin']}})
            .sort('createdAt')
            .exec(function(err, users){
                if(err){
                    console.log(err);
                }
                res.render('user.ejs', {
                    appname: config.appname,
                    user : req.user,
                    content: 'user',
                    data: users,
                    err: req.flash('err'),
                    success: req.flash('success'),
                    tips: req.flash('tips'),
                    warning: req.flash('warning')
                });
            });
    });

    app.get('/recordlog', isLoggedIn, isAdmin, function(req, res){
        User.find({'local.level': 'Member'},function(err, users){
            var newUsers = [];
            if(err){
                console.log(err);
            }
            users.forEach(function(user){
                newUsers.push({text: user.local.email, id: user._id});
            });
            res.render('recordlog.ejs', {
                appname: config.appname,
                user : req.user,
                content: 'recordlog',
                users: newUsers,
                err: req.flash('err'),
                success: req.flash('success'),
                tips: req.flash('tips'),
                warning: req.flash('warning')
            });
        });
    });

    app.get('/summary', isLoggedIn, isAdmin, function(req, res){
        res.render('summary.ejs', {appname: config.appname, user : req.user, content: 'summary'});

    });

    // post request process data

    app.post('/perencanaan', isLoggedIn, lastLogin, function(req, res) {
        var id = req.user._id;
        upload(req, res, function (err) {
            if (err) {
                console.log('render page with error');
                console.log(err);
            }

            var data = {
                description: req.body.keterangan,
                amount: parseInt(req.body.dana),
                date: new Date(req.body.tanggal),
                owner: id,
                category: req.body.kategoriDana
            };
            Neraca.create(data, function (err, neraca) {
                if (err) {
                    console.log(err);
                    fs.unlinkSync(file.path);
                }

                var file = req.file;
                if(file){
                    var acceptedExt = ['pdf','doc','docx','xls','xlsx','ppt','pptx','png','jpg','gif','bmp'];
                    if (acceptedExt.indexOf(getExt(file.originalname))=== -1) {
                        fs.unlinkSync(file.path);
                        req.flash('err',config.errorMessage.imageWrongFormat);
                        req.flash('tips','Record created without inserting proofing file');
                        insertLogs(req.user._id,config.stringCmd.create,null,neraca._id+' '+neraca.description+' '+neraca.amount);
                        return redirect('/perencanaan');
                    }
                    neraca.files.push(config.image.path+ '/' + file.filename);
                }

                neraca.save();
                req.flash('success','Record created');
                insertLogs(req.user._id,config.stringCmd.create,null,neraca._id+' '+neraca.description+' '+neraca.amount);
                res.redirect('/perencanaan');
            });
        });
    });

    app.get ('/perencanaan/delete/:id', isLoggedIn, lastLogin, function(req, res) {
        var recordid = req.params.id;
            Neraca.findById(recordid).exec(function(err,neraca){
                if(err){
                    req.flash('err','Failed delete record : ' + err.message);
                    console.log(err);
                }
                neraca.remove();
                insertLogs(req.user._id,config.stringCmd.delete,null,neraca._id+' '+neraca.description+' '+neraca.amount);
                req.flash('success','Success delete record');
                res.redirect('/perencanaan');
            });
    });

    app.post('/profile',isLoggedIn, lastLogin ,function(req, res) {
        var userId = req.user._id;
        User.findOne({ _id : userId },function(err,user){
            if(err){
                //return res.render('profile.ejs', {appname: config.appname, user : req.user, content: 'profile', err: err.message});
                req.flash('err',err.message);
                return res.redirect('/profile');
            }
            if(user == null){
                req.flash('err',config.errorMessage.userNotFound);
                return res.redirect('/profile');
            }
            upload(req, res, function (err) {
                if(req.body.name){
                    user.local.name = req.body.name;
                }
                if(req.body.password){
                    user.local.password = user.generateHash(req.body.password);
                }
                if (err) {
                    console.log('render page with error');
                    console.log(err);
                    user.save();
                    req.flash('err',err.message);
                    return res.redirect('/profile');
                }

                var file = req.file;
                if(file) {
                    var acceptedExt = ['png','jpg','gif','bmp'];
                    if (acceptedExt.indexOf(getExt(file.originalname)) === -1) {
                        fs.unlinkSync(file.path);
                        req.flash('err', config.errorMessage.imageWrongFormat);
                        insertLogs(req.user._id,config.stringCmd.updateProfile,null,null);
                        return res.redirect('/profile');
                    }
                    user.local.imagePath = config.image.path+ '/' + file.filename;
                }

                user.save();
                insertLogs(req.user._id,config.stringCmd.updateProfile,null,null);
                req.flash('success','Success Updating Profile');
                res.redirect('/profile');
            });
        });
    });

    app.post('/user',isLoggedIn,lastLogin, isAdmin, function(req, res){
        var data = {
            local: {email: req.body.email}
        };
        User.create(data, function(err, user){
            if(err){
                req.flash('err',err.message);
                console.log(err);
            }else{
                user.genPassword();
                req.flash('success','Success Creating Member');
                req.flash('tips','Password automatically send to registered email');
                insertLogs(req.user._id,config.stringCmd.create,user._id+' '+user.local.email,null);
            }
            res.redirect('/user');
        });
    });

    app.get('/user/:id',isLoggedIn,lastLogin,function(req, res){
        User.findById(req.params.id,function(err, user){
            res.send({err: err, user: user});
        });

    });

    app.get('/neraca/:id',isLoggedIn,lastLogin,function(req, res){
        Neraca.findById(req.params.id,function(err, neraca){
            res.send({err: err, neraca: neraca});
        });

    });

    app.get ('/user/delete/:id',isLoggedIn,lastLogin,isAdmin,function(req, res) {
        var userId = req.params.id;
        User.findById(userId).exec(function(err,user){
            if(err){
                req.flash('err','Failed delete member : ' + err.message);
                console.log(err);
            }else{
                Neraca.find({owner:userId}).remove();
                user.remove();
                req.flash('success','Success delete member '+user.local.name);
                req.flash('tips',user.local.name+'\'s records and logs automatically deleted');
                insertLogs(req.user._id,config.stringCmd.delete,user._id+' '+user.local.email,null);
            }
            res.redirect('/user');
        });
    });

    app.get ('/user/reset/:id',isLoggedIn,lastLogin,isAdmin, function(req, res) {
        var userId = req.params.id;
        User.findById(userId).exec(function(err,user){
            if(err){
                req.flash('err',err.message);
                console.log(err);
            }else{
                user.genPassword();
                req.flash('success','Success reset password member '+user.local.name);
                req.flash('tips','Password automatically send to registered email');
                insertLogs(req.user._id,config.stringCmd.resetPassword,user._id+' '+user.local.email,null);
            }
            res.redirect('/user');
        });
    });

    app.post('/recordlog',isLoggedIn,lastLogin, isAdmin, function(req, res){
        User.find({'local.level': 'Member'},function(err, users){
            var newUsers = [];
            if(err){
                console.log(err);
            }
            users.forEach(function(user){
                newUsers.push({text: user.local.email, id: user._id});
            });
            Neraca
                .find({owner: req.body.emailUser})
                //.sort('-createdAt')
                .sort('date')
                .exec(function(err, neracas) {
                    if (err) {
                        console.log(err);
                    }
                    var chartPendapatan = [];
                    var chartPembiayaan = [];
                    var chartPembelanjaan = [];
                    var totalPendapatan = 0;
                    var totalPembiayaan = 0;
                    var totalPembelanjaan = 0;
                    neracas.forEach(function(neraca){
                        if(neraca.category.charAt(0) == '1') {
                            totalPendapatan += parseInt(neraca.amount);
                            chartPendapatan.push({
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(totalPendapatan),
                                Keterangan: neraca.description,
                                Dana: totalPendapatan
                            });
                        } else if(neraca.category.charAt(0) == '2') {
                            totalPembiayaan += parseInt(neraca.amount);
                            chartPembiayaan.push({
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(totalPembiayaan),
                                Keterangan: neraca.description,
                                Dana: totalPembiayaan
                            });
                        }else if(neraca.category.charAt(0) == '3') {
                            totalPembelanjaan += parseInt(neraca.amount);
                            chartPembelanjaan.push({
                                y: helper.formatDate(neraca.date),
                                Duit: formatRupiah(totalPembelanjaan),
                                Keterangan: neraca.description,
                                Dana: totalPembelanjaan
                            });
                        }
                    });
                    res.render('recordlog.ejs', {
                        appname: config.appname,
                        user : req.user,
                        content: 'recordlog',
                        users: newUsers,
                        activeUser: req.body.emailUser,
                        chartPendapatan: chartPendapatan,
                        chartPembiayaan: chartPembiayaan,
                        chartPembelanjaan: chartPembelanjaan,
                        err: req.flash('err'),
                        success: req.flash('success'),
                        tips: req.flash('tips'),
                        warning: req.flash('warning')
                    });
                });
        });
    });

    app.post('/summary',isLoggedIn,lastLogin, isAdmin, function(req, res){
        res.redirect('/summary');
    });

    app.post('/buku',isLoggedIn,lastLogin, function(req, res){
        var id = req.user._id;
        var tahunSekarang = new Date(req.body.tahun,0,0);
        var tahunDepan = new Date(req.body.tahun+1,0,0);
        var filterData = {$match: {
            $and: [
                {owner : id},
                {date: { $gte: tahunSekarang,$lt: tahunDepan}}
        ]}};
        var grupData = {$group: {
            _id: '$category',
            total: {$sum: '$amount'},
            description: {$push: '$description'},
            countRecord: {$sum: 1}
        }};
        var sortData = {$sort: {_id: 1}};
        var aggregatePipe = [filterData,grupData,sortData];
        var category = [
            {text: 'Hasil Usaha', id: '1110'},
            {text: 'Swadaya, Partisipasi dan Gotong Royong', id: '1120'},
            {text: 'Lain-Lain Pendapatan Asli Desa yang Sah', id: '1130'},
            {text: 'Dana Desa', id: '1210'},
            {text: 'Bagian dari Hasil Pajak dan Retribusi Daerah Kabupaten / Kota', id: '1220'},
            {text: 'Alokasi Dana Desa', id: '1230'},
            {text: 'Bantuan Keuangan', id: '1240'},
            {text: 'Bantuan Provinsi', id: '1241'},
            {text: 'Bantuan Kabupaten / Kota', id: '1242'},
            {text: 'Hibah dan Sumbangan dari Pihak Ke-3 yang Tidak Mengikat', id: '1310'},
            {text: 'Lain-Lain Pendapatan Desa yang Sah', id: '1320'},
            {text: 'Belanja Pegawai (2111)', id: '2111'},
            {text: 'Belanja Barang dan Jasa (2122)', id: '2122'},
            {text: 'Belanja Modal (2123)', id: '2123'},
            {text: 'Belanja Barang dan Jasa (2132)', id: '2132'},
            {text: 'Belanja Barang dan Jasa (2142)', id: '2142'},
            {text: 'Belanja Barang dan Jasa (2212)', id: '2212'},
            {text: 'Belanja Modal (2213)', id: '2213'},
            {text: 'Belanja Barang dan Jasa (2222)', id: '2222'},
            {text: 'Belanja Modal (2223)', id: '2223'},
            {text: 'Kegiatan (2230)', id: '2230'},
            {text: 'Belanja Barang dan Jasa (2312)', id: '2312'},
            {text: 'Kegiatan (2320)', id: '2320'},
            {text: 'Belanja Barang dan Jasa (2412)', id: '2412'},
            {text: 'Kegiatan (2420)', id: '2420'},
            {text: 'Belanja Barang dan Jasa (2512)', id: '2512'},
            {text: 'Kegiatan (2520)', id: '2520'},
            {text: 'SILPA (3110)', id: '3110'},
            {text: 'Pencairan Dana Cadangan (3120)', id: '3120'},
            {text: 'Hasil Kekayaan Desa yang Dipisahkan (3130)', id: '3130'},
            {text: 'Pembentukan Dana Cadangan (3210)', id: '3210'},
            {text: 'Penyertaan Modal Desa (3220)', id: '3220'}
        ];


        var filterData2 = {$match: {owner: id}};
        var sortData2 = {$sort: {date: 1}};
        var grupData2 = {$group: {_id : {$year : '$date'}}};
        var aggregatePipe2 = [filterData2,grupData2,sortData2];
        Neraca.aggregate(aggregatePipe2,function(err, datas){
            if (err) {
                console.log(err)
            }
            var tahun = [];
            console.log(datas);
            datas.forEach(function(data){
                tahun.push({text: data._id.toString(), id: data._id.toString()});
            });
            Neraca.aggregate(aggregatePipe,function(err, neracas) {
                if (err) {
                    console.log(err)
                }
                var totalPendapatan = 0;
                var totalPembelanjaan = 0;
                var totalSurplus = 0;
                var totalDefisit = 0;
                neracas.forEach(function(neraca){
                    if(neraca._id.toString().charAt(0) == '1'){
                        totalPendapatan += neraca.total;
                    }
                    if(neraca._id.toString().charAt(0) == '2'){
                        totalPembelanjaan += neraca.total;
                    }
                    if(neraca._id.toString().charAt(0) == '3' && neraca._id.toString().charAt(1) == '1'){
                        totalSurplus += neraca.total;
                    }
                    if(neraca._id.toString().charAt(0) == '3' && neraca._id.toString().charAt(1) == '2'){
                        totalDefisit += neraca.total;
                    }
                    neraca.total = formatRupiah(neraca.total);
                    category.forEach(function(kategori){
                        if(neraca._id.toString() == kategori.id){
                            neraca.text = kategori.text;
                        }
                    });
                });
                console.log(neracas);
                res.render('buku.ejs', {
                    appname: config.appname,
                    user: req.user,
                    content: 'buku',
                    records: neracas,
                    pendapatan: formatRupiah(totalPendapatan),
                    pembelanjaan: formatRupiah(totalPembelanjaan),
                    surplus: formatRupiah(totalSurplus),
                    defisit: formatRupiah(totalDefisit),
                    activeTahun: req.body.tahun,
                    year: tahun
                });
            });
        });
    });
};

function formatRupiah(number){
    return accounting.formatMoney(number, "Rp ", 2, ".", ",");
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) {
        return next();
    }
    res.redirect('/login');
}

function isAdmin(req, res, next){
    if(req.user.local.level == 'Admin' || req.user.local.level == 'SuperAdmin' ){
        return next();
    }
    res.redirect('/');
}

function lastLogin(req, res, next){
    var clientIp = requestIp.getClientIp(req);
    var id = req.user._id;
    User.findById(id,function(err,user){
        if(err){
            console.log(err);
            return next();
        }
        user.lastLogin.path = req.url;
        user.lastLogin.ip = clientIp;
        user.lastLogin.date = new Date();
        user.lastLogin.userAgent = req.headers['user-agent'];
        user.save();
        return next();
    });
}

function getExt(filename){
    var str = filename.split('.');
    return str[str.length-1];
}

function insertLogs(userId, action, victimUser, victimNeraca){
    var data = {
        user: userId,
        action: action,
        victimUser: victimUser,
        victimNeraca: victimNeraca
    };
    Logs.create(data,function(err, log){
        if(err){
            console.log(err);
        }else{
            console.log('success insert log');
        }
    });
}