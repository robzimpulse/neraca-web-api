var mongoose            =   require('mongoose');
var Schema              =   mongoose.Schema;
var accounting = require('accounting');

var schema              =   new Schema({
    user                :   {
        type            :   Schema.Types.ObjectId,
        ref             :   'User'
    },
    action              :   String,
    victimUser          :   String,
    victimNeraca        :   String,
    createdAt           :   {
        type            :   Date,
        default         :   Date.now
    }
});

schema.set('toJSON', {
    virtuals: true
});

schema.virtual('amountString').get(function () {
    return accounting.formatMoney(this.amount, "Rp ", 2, ".", ",");
});

module.exports  =   mongoose.model('Logs',schema);