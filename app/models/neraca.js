var mongoose            =   require('mongoose');
var Schema              =   mongoose.Schema;
var accounting = require('accounting');

var schema              =   new Schema({
    description         :   {
        type            :   String,
        required        :   true
    },
    amount              :   {
        type            :   Number,
        required        :   true
    },
    files               :   [String],
    date                :   Date,
    category            :   {
        type            :   String,
        required        :   true
    },
    owner               :   {
        type            :   Schema.Types.ObjectId,
        ref             :   'User',
        required        :   true
    },
    createdAt           :   {
        type            :   Date,
        default         :   Date.now
    }
});

schema.set('toJSON', {
    virtuals: true
});

schema.set('toObject', {
    virtuals: true
});

schema.virtual('amountString').get(function () {
    return accounting.formatMoney(this.amount, "Rp ", 2, ".", ",");
});

schema.virtual('timeSince').get(function () {
    var now = new Date(Date.now());
    var time = this.createdAt;
    var diff = now - time;
    var data = {};

    var msec = diff;
    var hh = Math.floor(msec / 1000 / 60 / 60);
    msec -= hh * 1000 * 60 * 60;
    var mm = Math.floor(msec / 1000 / 60);
    msec -= mm * 1000 * 60;
    var ss = Math.floor(msec / 1000);
    msec -= ss * 1000;

    data.hours = hh;
    data.minutes = mm;
    data.seconds = ss;
    data.milliseconds = msec;

    return data;
});

module.exports  =   mongoose.model('Neraca',schema);