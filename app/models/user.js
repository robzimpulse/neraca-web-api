var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');
var Schema   =   mongoose.Schema;
var EmailTemplate = require('email-templates').EmailTemplate;
var helper = require('../../helper');
var config = helper.genConfig('app');
var credential = new EmailTemplate('./emailTemplate/accountCredential');
var nodemailer = require('nodemailer');
var mandrillTransport = require('nodemailer-mandrill-transport');
var transport = nodemailer.createTransport(mandrillTransport({auth: {apiKey: config.mandrillAuth.apiKey}}));

var userSchema = Schema({
    local                   :   {
        email               :   {
            type            :   String,
            unique          :   true,
            required        :   true
        },
        password            :   String,
        imagePath           :   {
            type            :   String,
            default         :   '/assets/dist/img/user2-160x160.jpg'
        },
        name                :   {
            type            :   String,
            default         :   'Unnamed'
        },
        level               :   {
            type            :   String,
            enum            :   ['SuperAdmin','Admin', 'Member'],
            default         :   'Member'
        },
        verified            :   {
            type            :   Boolean,
            default         :   false
        },
        verificationCode    :   {
            type            :   Number,
            select          :   false
        },
        createdAt           :   {
            type            :   Date,
            default         :   Date.now
        }
    },
    facebook                :   {
        id                  :   String,
        token               :   String,
        email               :   String,
        name                :   String
    },
    twitter                 :   {
        id                  :   String,
        token               :   String,
        displayName         :   String,
        username            :   String
    },
    google                  :   {
        id                  :   String,
        token               :   String,
        email               :   String,
        name                :   String
    },
    lastLogin               :   {
        ip                  :   String,
        date                :   Date,
        userAgent           :   String,
        path                :   String
    }
});

userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.genVerificationCode = function(){
    var high  = 999999,
        low   = 100001;
    return Math.floor(Math.random() * (high - low + 1) + low);
};

userSchema.methods.genPassword = function(){
    var random = this.genVerificationCode();
    this.sendPasswordByEmail(random);
    this.local.password = this.generateHash(random);
    this.local.verificationCode = this.genVerificationCode();
    this.sendVerificationByEmail();
    this.save();
};

userSchema.methods.creationDate = function(){
    var date = new Date(this.local.createdAt);
    return date.getDate();
};

userSchema.methods.sendPasswordByEmail = function(password){
    var user = {name: this.local.email.split('@')[0], email: this.local.email , password: password};
    var email = {
        from: 'no-reply@desaindonesia.org',
        to: this.local.email, // An array if you have multiple recipients.
        subject: 'desaindonesia Account Credential'
    };
    credential.render(user, function (err, results) {
        if(err){
            console.log(err);
        }else{
            email.html = results.html;
            transport.sendMail(email, function (err, info) {
                if (err) {
                    console.log('Error: ');
                    console.log(err)
                } else {
                    console.log('Info');
                    console.log(info);
                }
            });
        }

        // result.html
        // result.text
    });
};

userSchema.methods.verified = function(verificationCode){
    if(verificationCode == this.local.verificationCode){
        this.local.verificationCode = '';
        this.local.verified = true;
        this.save();
    }
};

userSchema.methods.sendVerificationByEmail = function(){
    console.log('Verification Code : '+this.local.verificationCode);
};

module.exports = mongoose.model('User', userSchema);