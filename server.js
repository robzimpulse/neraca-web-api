var express  = require('express');
var app      = express();
var path     = require('path');
var mongoose = require('mongoose');
var passport = require('passport');
var flash    = require('connect-flash');
var fs       = require('fs');
var logger       = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser   = require('body-parser');
var session      = require('express-session');
var helper       = require('./helper');
var credential   = helper.genConfig('credential');
var database     = helper.genConfig('database');
var config       = helper.genConfig('app');

var accessLogStream = fs.createWriteStream(__dirname + '/access.log', {flags: 'a'});

mongoose.connect(database.url);

require('./config/passport')(passport);

app.use(logger('dev', {stream: accessLogStream}));
app.use(bodyParser());
app.use(cookieParser());
app.set('view engine', 'ejs');
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({ secret: credential.secret }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./app/routes.js')(app, passport);

app.listen(config.port);
console.log('Server start at port : ' + config.port + '['+new Date(Date.now())+']');