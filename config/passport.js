var LocalStrategy   = require('passport-local').Strategy;
var User            = require('../app/models/user');
module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use('local-login', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true},
        function(req, email, password, done) {
            User.findOne({ 'local.email' :  email }, function(err, user) {
                if (err)
                    return done(err);
                if (!user)
                    return done(null, false, req.flash('loginMessage', 'No user found.'));
                if (!user.validPassword(password))
                    return done(null, false, req.flash('loginMessage', 'Oops! Wrong password.'));
                console.log('['+user.local.name+'][Login]['+new Date(Date.now())+']');
                return done(null, user);
            });
        }
    ));

    passport.use('local-signup', new LocalStrategy({
        usernameField : 'email',
        passwordField : 'password',
        passReqToCallback : true},
        function(req, email, password, done) {
            if(req.body.password != req.body.password2)
                return done(null, false, req.flash('signupMessage', 'Retype password not identical'));
            process.nextTick(function () {
                User.findOne({'local.email': email}, function (err, user) {
                    if (err)
                        return done(err);
                    if (user) {
                        return done(null, false, req.flash('signupMessage', 'Email is already taken.'));
                    } else {
                        var newUser = new User();
                        newUser.local.email = email;
                        newUser.local.password = newUser.generateHash(password);
                        newUser.local.verificationCode = newUser.genVerificationCode();
                        newUser.save(function (err) {
                            if (err)
                                throw err;
                            console.log('['+newUser.local.name+'][Signup]['+new Date(Date.now())+']');
                            return done(null, newUser);
                        });
                    }
                });
            });
        }
    ));
};


