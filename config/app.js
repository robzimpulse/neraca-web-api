module.exports = {
    appname : 'sisdes',
    port: 80,
    errorMessage:{
        userNotFound: 'Sorry, User Not Found',
        imageWrongFormat: 'Sorry, Unsupported file format'
    },
    image:{
        path: '/files'
    },
    stringCmd: {
        delete: 'Delete',
        updateProfile: 'Update Profile',
        create: 'Create',
        resetPassword: 'Reset Password'
    },
    mandrillAuth: {
        apiKey: 'JT4fxNNFNOQ59LPfs2Wf6g'
    }
};