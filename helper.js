var fs = require('fs');
require('magic-globals');

module.exports  =   {
    genConfig : function(index){
        var routes = {};
        var routes_path = __base + '/config';
        fs.readdirSync(routes_path).forEach(function (file) {
            if (file.indexOf('.js') != -1) {
                routes[file.split('.')[0]] = require(routes_path + '/' + file);
            }
        });
        return routes[index];
    },
    genModels : function(index){
        var models = {};
        var models_path = __base + '/app/models';
        fs.readdirSync(models_path).forEach(function (file) {
            if (file.indexOf('.js') != -1) {
                models[file.split('.')[0]] = require(models_path + '/' + file);
            }
        });
        return models[index];
    },
    formatDate : function(date){
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [year, month, day].join('-');
    }
};